import React from 'react';
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import TimeSinceDisplay from './src/components/TimeSinceDisplay';
import PopupDialog from 'react-native-popup-dialog';

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            events: [{
                key: 0,
                name: "I made this app",
                startDatetime: "2018-03-14"
            }, {
                key: 1,
                name: "The new year started",
                startDatetime: "2018-01-01"
            }]
        };

        this.removeEvent = this.removeEvent.bind(this);
        this.handleEventPressed = this.handleEventPressed.bind(this);
        this.handleAddPressed = this.handleAddPressed.bind(this);
    }

    render() {
        // Create an array of TimeSinceDisplays
        let eventsJSX = this.state.events.map((event, key) =>
            <TimeSinceDisplay event={event} key={key} handleEventPressed={this.handleEventPressed}/>
        );

        // main jsx
        return (
            <View style={styles.container}>
                <View>
                    {eventsJSX}
                    <TouchableOpacity style={styles.addEventTouchable} onPress={this.handleAddPressed}>
                        <Text>Add Event</Text>
                    </TouchableOpacity>
                </View>
                <PopupDialog ref={(popupDialog) => this.popupDialog = popupDialog }>
                    <View>
                        <Text>Hello</Text>
                    </View>
                </PopupDialog>
            </View>
        );
    }

    handleEventPressed(event) {
        this.popupDialog.show();
    }

    handleAddPressed() {
        let event = {
            name: "What happened?",
            startDatetime: "When?"
        };
        this.setState({
            events: this.state.events.concat(event)
        });
    }

    removeEvent(event) {
        this.setState({
            events: this.state.events.filter((e) => e !== event)
        });
    }

    editEvent(event) {

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    addEventTouchable: {
        backgroundColor: '#65a3bc',
        width: 300,
        borderWidth: 5,
        borderColor: 'black',
        borderStyle: 'solid',
        padding: 10,
        margin: 5
    }
});
