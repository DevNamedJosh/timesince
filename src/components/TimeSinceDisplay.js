import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Button} from 'react-native';
import moment from 'moment';

export default class TimeSinceDisplay extends React.Component {
    render() {
        return (
            <TouchableOpacity style={styles.box} onPress={() => this.props.handleEventPressed(this.event)}>
                <Text>{this.props.event.name}</Text>
                <Text>{this.calculateAndFormatTimeSinceDatetime(this.props.event.startDatetime)}</Text>
            </TouchableOpacity>
        );
    }

    calculateAndFormatTimeSinceDatetime(datetime) {
        return moment(datetime).fromNow();
    }
}

const styles = StyleSheet.create({
    box: {
        backgroundColor: '#bc7fb5',
        width: 300,
        borderWidth: 5,
        borderColor: 'black',
        borderStyle: 'solid',
        padding: 10,
        margin: 5
    }
});
